import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import * as t from './news';

const middlewares = [thunk];

const reduxInitialState = {
  news: null,
  isLoading: false,
  errorMessage: '',
};
const store = configureMockStore(middlewares)(reduxInitialState);

describe('news actions', () => {
  afterEach(() => {
    fetchMock.restore();
    store.clearActions();
  });

  const endpoint = 'path:/api/v1/news';

  it('on success actions is NEWS_REQUEST NEWS_SUCCESS', async () => {
    fetchMock.getOnce(endpoint, {
      body: { status: 'ok', data: [1, 2, 3] },
      headers: { 'content-type': 'application/json' },
    });

    const expectedActions = [
      { type: t.NEWS_REQUEST },
      {
        type: t.NEWS_SUCCESS,
        payload: [1, 2, 3],
      },
    ];

    await store.dispatch(t.getAll());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('on failure actions is NEWS_REQUEST NEWS_FAILURE', async () => {
    fetchMock.getOnce(endpoint, {
      body: { status: 'error', message: 'mock server error' },
      headers: { 'content-type': 'application/json' },
    });

    const expectedActions = [
      { type: t.NEWS_REQUEST },
      {
        type: t.NEWS_FAILURE,
        payload: 'mock server error',
      },
    ];

    await store.dispatch(t.getAll());
    expect(store.getActions()).toEqual(expectedActions);
  });

  it('on failure actions with network error', async () => {
    fetchMock.getOnce(endpoint, () => {
      throw new Error();
    });

    const expectedActions = [
      { type: t.NEWS_REQUEST },
      {
        type: t.NEWS_FAILURE,
        payload: 'unknown_error',
      },
    ];

    await store.dispatch(t.getAll());
    expect(store.getActions()).toEqual(expectedActions);
  });
});
