import * as userService from '../services/user';

export const USER_LOGOUT = 'USER_LOGOUT';

export const USER_LOGIN_REQUEST = 'USER_LOGIN_REQUEST';
export const USER_LOGIN_SUCCESS = 'USER_LOGIN_SUCCESS';
export const USER_LOGIN_FAILURE = 'USER_LOGIN_FAILURE';

export const USER_INFO_REQUEST = 'USER_INFO_REQUEST';
export const USER_INFO_SUCCESS = 'USER_INFO_SUCCESS';
export const USER_INFO_FAILURE = 'USER_INFO_FAILURE';

const loginInRequestAction = () => ({
  type: USER_LOGIN_REQUEST,
});

const loginSuccessAction = payload => ({
  type: USER_LOGIN_SUCCESS,
  payload,
});

const loginFailureAction = (payload = 'unknown_error') => ({
  type: USER_LOGIN_FAILURE,
  payload,
});

const userInfoRequestAction = () => ({
  type: USER_INFO_REQUEST,
});

const userInfoSuccessAction = payload => ({
  type: USER_INFO_SUCCESS,
  payload,
});

const userInfoFailureAction = (payload = 'unknown_error') => ({
  type: USER_INFO_FAILURE,
  payload,
});

export const userLogin = (email, password) => async dispatch => {
  dispatch(loginInRequestAction());

  try {
    const response = await userService.login(email, password);
    if (response.status === 'ok') {
      dispatch(loginSuccessAction(response.data));
    } else {
      dispatch(loginFailureAction(response.message));
    }
  } catch (error) {
    dispatch(loginFailureAction());
  }
};

export const userLogout = () => ({
  type: USER_LOGOUT,
});

export const getUserInfo = userId => async dispatch => {
  dispatch(userInfoRequestAction());

  try {
    const response = await userService.getUserInfo(userId);
    if (response.status === 'ok') {
      dispatch(userInfoSuccessAction(response.data));
    } else {
      dispatch(userInfoFailureAction(response.message));
    }
  } catch (error) {
    dispatch(userInfoFailureAction());
  }
};
