import configureMockStore from 'redux-mock-store';
import thunk from 'redux-thunk';
import fetchMock from 'fetch-mock';

import * as t from './user';

const middlewares = [thunk];
const reduxInitialStore = {
  userId: 0,
  isLoading: false,
  isAuthorized: false,
  errorMessage: '',

  isUserInfoLoading: false,
  userInfo: {},
};
const store = configureMockStore(middlewares)(reduxInitialStore);

const loginEndpoint = 'path:/api/v1/validate';
const userInfoEndpoint = 'express:/api/v1/user-info/:id';

describe('user actions', () => {
  afterEach(() => {
    fetchMock.restore();
    store.clearActions();
  });

  describe('user login actions', () => {
    it('user login success', async () => {
      fetchMock.postOnce(loginEndpoint, {
        body: { status: 'ok', data: { id: 2 } },
        headers: { 'content-type': 'application-json' },
      });
      const expectedActions = [
        { type: t.USER_LOGIN_REQUEST },
        { type: t.USER_LOGIN_SUCCESS, payload: { id: 2 } },
      ];

      await store.dispatch(t.userLogin('abc', '123'));

      const request = fetchMock.lastCall()[1];

      expect(JSON.parse(request.body)).toEqual({ email: 'abc', password: '123' });
      expect(store.getActions()).toEqual(expectedActions);
    });

    it('user login failure', async () => {
      fetchMock.postOnce(loginEndpoint, {
        body: { status: 'error', message: 'some login error' },
        headers: { 'content-type': 'application-json' },
      });
      const expectedActions = [
        { type: t.USER_LOGIN_REQUEST },
        { type: t.USER_LOGIN_FAILURE, payload: 'some login error' },
      ];

      await store.dispatch(t.userLogin('abc', '123'));
      expect(store.getActions()).toEqual(expectedActions);

    });

    it('user login network failure', async () => {
      fetchMock.postOnce(loginEndpoint, () => {
        throw new Error();
      });
      
      const expectedActions = [
        { type: t.USER_LOGIN_REQUEST },
        { type: t.USER_LOGIN_FAILURE, payload: 'unknown_error' },
      ];

      await store.dispatch(t.userLogin('abc', '123'));
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('user logout sync actions', () => {
    it('check logout action', () => {
      const expectedActions = [
        {type: t.USER_LOGOUT}
      ];

      store.dispatch(t.userLogout());
      expect(store.getActions()).toEqual(expectedActions);
    });
  });

  describe('user info actions', () => {
    it('user info success async action', async () => {
      fetchMock.getOnce(userInfoEndpoint, {
        body: { status: 'ok', data: {'abc': 'def'} },
        headers: { 'content-type': 'application-json' },
      });
      const expectedActions = [
        { type: t.USER_INFO_REQUEST },
        { type: t.USER_INFO_SUCCESS, payload: {'abc': 'def'} },
      ];

      await store.dispatch(t.getUserInfo(1));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('user info failure async action', async () => {
      fetchMock.getOnce(userInfoEndpoint, {
        body: { status: 'error', message: 'some server error' },
        headers: { 'content-type': 'application-json' },
      });
      const expectedActions = [
        { type: t.USER_INFO_REQUEST },
        { type: t.USER_INFO_FAILURE, payload: 'some server error' },
      ];

      await store.dispatch(t.getUserInfo(1));

      expect(store.getActions()).toEqual(expectedActions);
    });

    it('user info network async failure action', async () => {
      fetchMock.getOnce(userInfoEndpoint, () => {
        throw new Error();
      });
      const expectedActions = [
        { type: t.USER_INFO_REQUEST },
        { type: t.USER_INFO_FAILURE, payload: 'unknown_error' },
      ];

      await store.dispatch(t.getUserInfo(1));

      expect(store.getActions()).toEqual(expectedActions);
    });
  });
});
