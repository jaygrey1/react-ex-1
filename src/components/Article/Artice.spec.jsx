import React from 'react';
import { shallow } from 'enzyme';

import Article from './index';

describe('Article component', () => {
  it('renders correctly', () => {
    const props = {
      data: {
        title: 'article title',
        text: 'article text',
      },
    };
    const component = shallow(<Article {...props} />);
    expect(component).toMatchSnapshot();
  });
});
