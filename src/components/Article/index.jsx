import React from 'react';
import { PropTypes } from 'prop-types';

import './Article.css';

const Article = props => {
  const { data } = props;

  return (
    <div className="article">
      <div className="title">{data.title}</div>
      <div className="content">{data.text}</div>
    </div>
  );
};

Article.propTypes = {
  data: PropTypes.shape({
    title: PropTypes.string.isRequired,
    text: PropTypes.string.isRequired,
  }).isRequired,
};

export default Article;
