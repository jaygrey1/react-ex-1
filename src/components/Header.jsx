import React from 'react';
import { NavLink, withRouter } from 'react-router-dom';
import PropTypes from 'prop-types';

import './header.css';

export const Header = ({ isAuthorized, logoutHandler, history }) => {
  const button = !isAuthorized ? (
    <button
      type="button"
      onClick={() => {
        history.push('/login');
      }}
    >
      login
    </button>
  ) : (
    <button type="button" onClick={logoutHandler}>
      logout
    </button>
  );

  return (
    <header>
      <ul>
        <li>
          <NavLink to="/" exact activeClassName="activeMenuElement">
            Home
          </NavLink>
        </li>
        <li>
          <NavLink to="/news" activeClassName="activeMenuElement">
            News
          </NavLink>
        </li>
        <li>
          <NavLink to="/profile" activeClassName="activeMenuElement">
            Profile
          </NavLink>
        </li>
      </ul>
      <span>{button}</span>
    </header>
  );
};

Header.propTypes = {
  history: PropTypes.objectOf(PropTypes.any).isRequired,
  isAuthorized: PropTypes.bool.isRequired,
  logoutHandler: PropTypes.func.isRequired,
};

export default withRouter(Header);
