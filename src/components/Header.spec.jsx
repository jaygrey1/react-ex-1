import React from 'react';
import { shallow } from 'enzyme';

import { Header } from './Header';

describe('Header component', () => {
  it('matches snapshot', () => {
    const props = {
      isAuthorized: false,
      logoutHandler: () => {},
      history: {},
    };

    const component = shallow(<Header {...props} />);

    expect(component).toMatchSnapshot();
  });
  it('has 3 links', () => {
    const props = {
      isAuthorized: false,
      logoutHandler: () => {},
      history: {},
    };

    const component = shallow(<Header {...props} />);
    expect(component.find('NavLink')).toHaveLength(3);
  });

  it('button change text', () => {
    const props = {
      isAuthorized: false,
      logoutHandler: () => {},
      history: {},
    };

    const component = shallow(<Header {...props} />);
    expect(
      component
        .find('button')
        .first()
        .text()
    ).toBe('login');

    const nextProps = {
      ...props,
      isAuthorized: true,
    };
    component.setProps(nextProps);
    expect(
      component
        .find('button')
        .first()
        .text()
    ).toBe('logout');
  });

  it('it calls logoutHandler on logout', () => {
    const logoutHandler = jest.fn();
    const props = {
      isAuthorized: true,
      logoutHandler,
      history: {},
    };

    const component = shallow(<Header {...props} />);
    component.find('button').first().simulate('click');
    expect(logoutHandler).toHaveBeenCalledTimes(1);
  });
});
