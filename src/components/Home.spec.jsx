import React from 'react';

import { shallow } from 'enzyme';
import Home from './Home';

describe('Home component', () => {
  it('matches snapshot', () => {
    const component = shallow(<Home />);
    expect(component).toMatchSnapshot();
  });

  it('renders correctly', () => {
    const component = shallow(<Home />);
    expect(component.find('p')).toHaveLength(1);
    expect(
      component
        .find('p')
        .first()
        .text()
    ).toBe('Home page');
  });
});
