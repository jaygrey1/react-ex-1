import React from 'react';
import { shallow } from 'enzyme';
import Login from './index';
import { getMessage } from '../../helpers/messages';

describe('Login component', () => {
  it('renders correctly', () => {
    const props = {
      login: jest.fn(),
      isLoading: false,
      errorMessage: '',
    };
    const component = shallow(<Login {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('blocks inputs and submit button when loading', () => {
    const props = {
      login: jest.fn(),
      isLoading: true,
      errorMessage: '',
    };
    const component = shallow(<Login {...props} />);
    expect(
      component
        .find('#username')
        .first()
        .prop('disabled')
    ).toBe(true);

    expect(
      component
        .find('#password')
        .first()
        .prop('disabled')
    ).toBe(true);

    expect(
      component
        .find('button')
        .first()
        .prop('disabled')
    ).toBe(true);

    expect(
      component
        .find('button')
        .first()
        .text()
    ).toBe('wait...');
  });

  it('displays error message in case of error', () => {
    const props = {
      login: jest.fn(),
      isLoading: false,
      errorMessage: 'unknown_error',
    };
    const component = shallow(<Login {...props} />);
    expect(component.find('p').text()).toEqual(getMessage(props.errorMessage));
  });

  it('calls login function on submit', () => {
    const mockLogin = jest.fn();

    const props = {
      login: mockLogin,
      isLoading: false,
      errorMessage: '',
    };

    const component = shallow(<Login {...props} />);
    component.find('button').simulate('click');
    expect(mockLogin).toHaveBeenCalledTimes(1);
  });

  it('checks isEmailValid state', () => {
    const props = {
      login: jest.fn(),
      isLoading: false,
      errorMessage: '',
    };

    const component = shallow(<Login {...props} />);

    component.find('#username').simulate('change', { currentTarget: { value: 'abcdef' } });
    expect(component.state('isEmailValid')).toBe(false);

    component.find('#username').simulate('change', { currentTarget: { value: 'max@test.com' } });
    expect(component.state('isEmailValid')).toBe(true);
  });

  it('clears password field on failure login', () => {
    const props = {
      login: jest.fn(),
      isLoading: false,
      errorMessage: '',
    };

    const component = shallow(<Login {...props} />);
    component.find('#password').simulate('change', { currentTarget: { value: 'secret' } });
    component.setProps({ errorMessage: 'unknown_error' });
    expect(component.find('#password').props().value).toBe('');
  });
});
