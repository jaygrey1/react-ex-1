import React from 'react';
import { PropTypes } from 'prop-types';
import validator from 'validator';

import { getMessage } from '../../helpers/messages';

import './login.css';

class Login extends React.Component {
  constructor(props) {
    super(props);

    this.state = {
      username: '',
      password: '',
      isEmailValid: true,
    };

    this.usernameRef = React.createRef();
  }

  componentDidMount() {
    if (this.usernameRef.current) {
      this.usernameRef.current.focus();
    }
  }

  componentDidUpdate(prevProps) {
    if (this.props.errorMessage !== prevProps.errorMessage) {
      // eslint-disable-next-line react/no-did-update-set-state
      this.setState({ password: '' });
    }
  }

  handleLogin = () => {
    const { username, password, isEmailValid } = this.state;
    const { login } = this.props;

    if (isEmailValid) {
      login(username, password);
    }
  };

  handleChangeUsername = e =>
    this.setState({
      username: e.currentTarget.value,
      isEmailValid: validator.isEmail(e.currentTarget.value),
    });

  handleChangePassword = e => this.setState({ password: e.currentTarget.value });

  handleKeyPressPassword = ({ key }) => {
    if (!this.props.isLoading && key === 'Enter') {
      this.handleLogin();
    }
  };

  render() {
    const { errorMessage, isLoading } = this.props;
    const { isEmailValid, username, password } = this.state;

    return (
      <div id="container">
        <form>
          <h3>Log-in</h3>
          <input
            className={isEmailValid ? '' : 'emailError'}
            type="text"
            name="username"
            id="username"
            placeholder="e-mail"
            ref={this.usernameRef}
            value={username}
            disabled={isLoading}
            onChange={this.handleChangeUsername}
          />
          <input
            type="password"
            name="password"
            id="password"
            placeholder="password"
            value={password}
            disabled={isLoading}
            onChange={this.handleChangePassword}
            onKeyPress={this.handleKeyPressPassword}
          />
          <button type="button" disabled={isLoading || !isEmailValid} onClick={this.handleLogin}>
            {isLoading ? 'wait...' : 'login'}
          </button>
          <p>{getMessage(errorMessage)}</p>
        </form>
      </div>
    );
  }
}

Login.propTypes = {
  login: PropTypes.func.isRequired,
  isLoading: PropTypes.bool.isRequired,
  errorMessage: PropTypes.string.isRequired,
};

export default Login;
