import React from 'react';

import { shallow } from 'enzyme';
import News from './index';

describe('News component', () => {
  it('renders correctly', () => {
    const props = {
      news: [
        { id: 1, title: '1', text: '1' },
        { id: 2, title: '2', text: '2' },
        { id: 3, title: '3', text: '3' },
      ],
    };
    const component = shallow(<News {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('it doesnt show when no news', () => {
    const props = {
      news: null,
    };
    const component = shallow(<News {...props} />);
    expect(component.find('Article')).toHaveLength(0);
    expect(component.find('.newsTotal')).toHaveLength(0);
  });
});
