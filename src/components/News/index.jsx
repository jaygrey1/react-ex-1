import React from 'react';
import PropTypes from 'prop-types';
import Article from '../Article/index';

const renderArticles = news => {
  if (!news || news.length === 0) {
    return null;
  }

  const articles = news.map(article => <Article data={article} key={article.id} />);

  return articles;
};

const News = ({ news }) => {
  if (news && news.length > 0) {
    return (
      <React.Fragment>
        {renderArticles(news)}
        <p className="newsTotal">всего новостей {news.length}</p>
      </React.Fragment>
    );
  }

  return null;
};

News.propTypes = {
  news: PropTypes.arrayOf(PropTypes.any),
};

News.defaultProps = {
  news: [],
};

export default News;
