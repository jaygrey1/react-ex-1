import React from 'react';
import { shallow } from 'enzyme';

import Page from './index';
import { getMessage } from '../../helpers/messages';

describe('Page component', () => {
  const defaultProps = {
    isLoading: false,
    errorMessage: '',
    component: {},
  };

  it('renders correctly', () => {
    const component = shallow(<Page {...defaultProps} />);
    expect(component).toMatchSnapshot();
  });

  it('shows spinner on loading', () => {
    const props = {
      ...defaultProps,
      isLoading: true,
    };

    const component = shallow(<Page {...props} />);
    expect(component.find('Spinner')).toHaveLength(1);
    expect(component.find('.pageError')).toHaveLength(0);
    expect(component.find('.page')).toHaveLength(0);
  });

  it('shows error message on failure', () => {
    const props = {
      ...defaultProps,
      errorMessage: 'unknown_error',
    };
    const component = shallow(<Page {...props} />);
    expect(component.find('Spinner')).toHaveLength(0);
    expect(component.find('.pageError')).toHaveLength(1);
    expect(component.find('.pageError').text()).toBe(getMessage(props.errorMessage));
    expect(component.find('.page')).toHaveLength(0);
  });

  it('shows component inside div.page', () => {
    const component = shallow(<Page {...defaultProps} />);
    expect(component.find('Spinner')).toHaveLength(0);
    expect(component.find('.pageError')).toHaveLength(0);
    expect(component.find('div.page')).toHaveLength(1);
  });
});
