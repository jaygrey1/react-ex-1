import React from 'react';
import PropTypes from 'prop-types';

import Spinner from '../Spinner/index';
import { getMessage } from '../../helpers/messages';

const Page = ({ isLoading, errorMessage, component }) => {
  if (isLoading) {
    return <Spinner />;
  }

  if (errorMessage.length > 0) {
    if (errorMessage.length > 0) {
      return <div className="pageError">{getMessage(errorMessage)}</div>;
    }
  }

  return <div className="page">{component}</div>;
};

Page.propTypes = {
  isLoading: PropTypes.bool,
  errorMessage: PropTypes.string,
  component: PropTypes.objectOf(PropTypes.any).isRequired,
};

Page.defaultProps = {
  isLoading: false,
  errorMessage: '',
};

export default Page;
