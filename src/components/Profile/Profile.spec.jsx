import React from 'react';
import { shallow } from 'enzyme';

import Profile from './index';

describe('Profile component', () => {
  it('renders correctly', () => {
    const props = {
      userInfo: {
        userId: 1,
        city: 'city',
        languages: ['lang1', 'lang2'],
        social: [{ label: '1', link: '1' }, { label: '2', link: '2' }],
      },
    };

    const component = shallow(<Profile {...props} />);
    expect(component).toMatchSnapshot();
  });

  it('web link must be first', () => {
    const props = {
      userInfo: {
        userId: 1,
        city: 'city',
        languages: ['lang1', 'lang2'],
        social: [
          { label: '1', link: '1' },
          { label: '2', link: '2' },
          { label: 'web', link: 'weblink' },
        ],
      },
    };

    const component = shallow(<Profile {...props} />);
    expect(
      component
        .find('.profileSocialLinks li')
        .first()
        .text()
    ).toBe('web');
  });
});
