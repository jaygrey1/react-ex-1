import React from 'react';
import PropTypes from 'prop-types';

const renderLanguages = (langs = []) => {
  if (langs.length === 0) {
    return null;
  }
  const items = langs.map(lang => <li key={lang}>{lang}</li>);
  return <ul>{items}</ul>;
};

const renderSocialLinks = (links = []) => {
  if (links.length === 0) {
    return null;
  }
  const webLink = links.filter(item => item.label === 'web');
  const others = links.filter(item => item.label !== 'web');
  const reorganizedLinks = [...webLink, ...others];

  const items = reorganizedLinks.map(item => (
    <li key={item.label}>
      <img src={`./icons/${item.label}.png`} alt={item.label} />
      <a href={item.link} target="_blank" rel="noreferrer noopener">
        {item.label}
      </a>
    </li>
  ));

  return <ul>{items}</ul>;
};

const Profile = ({ userInfo }) => {
  return (
    <div className="profileContent">
      <div>
        <h3>Город: {userInfo.city}</h3>
      </div>

      <div className="profileLanguages">
        <h3>Знание языков:</h3>
        {renderLanguages(userInfo.languages)}
      </div>

      <div className="profileSocialLinks">
        <h3>Ссылки:</h3>
        {renderSocialLinks(userInfo.social)}
      </div>
    </div>
  );
};

Profile.propTypes = {
  userInfo: PropTypes.shape({
    userId: PropTypes.number,
    city: PropTypes.string,
    languages: PropTypes.arrayOf(PropTypes.string),
    social: PropTypes.arrayOf(
      PropTypes.shape({ label: PropTypes.string.isRequired, link: PropTypes.string.isRequired })
    ),
  }).isRequired,
};

export default Profile;
