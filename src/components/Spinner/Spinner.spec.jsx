import React from 'react';
import { shallow } from 'enzyme';

import Spinner from './index';

describe('Spinner component', () => {
  it('renders correctly', () => {
    const component = shallow(<Spinner />);
    expect(component).toMatchSnapshot();
  });
});
