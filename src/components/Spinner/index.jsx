import React from 'react';
import './Spinner.css';

const Spinner = () => {
  return (
    <div className="spinnerWrapper">
      <div className="spinner" />
    </div>
  );
};

export default Spinner;
