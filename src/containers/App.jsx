import React from 'react';
import { BrowserRouter, Route } from 'react-router-dom';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import PageHeader from '../components/Header';
import Page from '../components/Page/index';
import Home from '../components/Home';
import LoginPage from './LoginPage/index';
import NewsPage from './NewsPage/index';
import ProfilePage from './ProfilePage/index';
import PrivateRoute from './PrivateRoute/index';
import { userLogout } from '../actions/user';

import './App.css';

const App = props => {
  const { isAuthorized } = props;

  return (
    <BrowserRouter>
      <div className="App">
        <PageHeader isAuthorized={isAuthorized} logoutHandler={props.userLogout} />
        <Route exact path="/" render={() => <Page component={<Home />} />} />
        <Route exact path="/news" component={NewsPage} />
        <PrivateRoute exact path="/profile" component={ProfilePage} />
        <Route exact path="/login" component={LoginPage} />
      </div>
    </BrowserRouter>
  );
};

App.propTypes = {
  isAuthorized: PropTypes.bool.isRequired,
  userLogout: PropTypes.func.isRequired,
};

const mapStateToProps = state => {
  return {
    isAuthorized: state.user.isAuthorized,
  };
};

const mapDispatchToProps = {
  userLogout,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(App);
