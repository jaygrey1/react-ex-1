import React from 'react';
import { shallow } from 'enzyme';

import { LoginPage } from './index';

describe('LoginPage container', () => {
  const defaultProps = {
    isLoading: false,
    login: jest.fn(),
    errorMessage: '',
    isAuthorized: false,
  };
  it('renders Login component if is not authorized', () => {
    const component = shallow(<LoginPage {...defaultProps} />);
    expect(component.find('Redirect')).toHaveLength(0);
    expect(component.find('Page')).toHaveLength(1);
  });

  it('redirects to "/profile" if authorized', () => {
    const props = {
      ...defaultProps,
      isAuthorized: true,
    };

    const component = shallow(<LoginPage {...props} />);
    expect(component.find('Redirect')).toHaveLength(1);
    expect(
      component
        .find('Redirect')
        .first()
        .prop('to')
    ).toBe('/profile');
    expect(component.find('Page')).toHaveLength(0);
  });
});
