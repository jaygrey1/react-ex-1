import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import { Redirect } from 'react-router-dom';

import Page from '../../components/Page/index';
import Login from '../../components/Login/index';
import { userLogin } from '../../actions/user';

export const LoginPage = props => {
  return props.isAuthorized ? (
    <Redirect to="/profile" />
  ) : (
    <Page component={<Login {...props} />} />
  );
};

LoginPage.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  login: PropTypes.func.isRequired,
  errorMessage: PropTypes.string.isRequired,
  isAuthorized: PropTypes.bool.isRequired,
};

const mapStateToProps = state => ({
  isLoading: state.user.isLoading,
  isAuthorized: state.user.isAuthorized,
  errorMessage: state.user.errorMessage,
});

const dispatchStateToProps = {
  login: userLogin,
};

export default connect(
  mapStateToProps,
  dispatchStateToProps
)(LoginPage);
