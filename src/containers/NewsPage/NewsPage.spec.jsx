import React from 'react';
import { shallow } from 'enzyme';
import { NewsPage } from '.';

describe('NewsPage container', () => {
  it('renders properly when news is null', () => {
    const props = {
      isLoading: false,
      getAll: jest.fn(),
      news: null,
    };

    const component = shallow(<NewsPage {...props} />);
    expect(component).toMatchSnapshot();
  });
});
