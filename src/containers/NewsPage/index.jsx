import React from 'react';
import { connect } from 'react-redux';
import { PropTypes } from 'prop-types';
import Page from '../../components/Page/index';
import News from '../../components/News/index';
import { getAll } from '../../actions/news';

export class NewsPage extends React.Component {
  componentDidMount() {
    const { news } = this.props;
    if (!news || news.length === 0) {
      this.props.getAll();
    }
  }

  render() {
    const { isLoading, news } = this.props;
    return <Page isLoading={isLoading} component={<News news={news} />} />;
  }
}

NewsPage.propTypes = {
  isLoading: PropTypes.bool.isRequired,
  news: PropTypes.arrayOf(PropTypes.shape()),
  getAll: PropTypes.func.isRequired,
};

NewsPage.defaultProps = {
  news: [],
};

const mapStateToProps = store => ({
  isLoading: store.news.isLoading,
  news: store.news.news,
});

const dispatchStateToProps = {
  getAll,
};

export default connect(
  mapStateToProps,
  dispatchStateToProps
)(NewsPage);
