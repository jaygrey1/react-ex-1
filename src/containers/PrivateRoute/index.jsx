import React from 'react';
import PropTypes from 'prop-types';

import { Route, Redirect } from 'react-router-dom';
import { connect } from 'react-redux';

const PrivateRoute = ({ userId, component: Component, ...rest }) => {
  return (
    <Route
      {...rest}
      render={props => (userId === 0 ? <Redirect to="/login" /> : <Component {...props} />)}
    />
  );
};

PrivateRoute.propTypes = {
  userId: PropTypes.number,
  component: PropTypes.objectOf(PropTypes.any).isRequired,
};

PrivateRoute.defaultProps = {
  userId: 0,
};

const mapStateToProps = store => ({
  userId: store.user.userId,
});

export default connect(mapStateToProps)(PrivateRoute);
