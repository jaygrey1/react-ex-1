import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { getUserInfo } from '../../actions/user';
import Page from '../../components/Page/index';

import Profile from '../../components/Profile/index';

import './Profile.css';

class ProfilePage extends React.Component {
  componentDidMount() {
    const { isAuthorized, userId, userInfo } = this.props;
    if (isAuthorized && !userInfo.userId) {
      this.props.getUserInfo(userId);
    }
  }

  render() {
    const { isUserInfoLoading, userInfo, errorMessage } = this.props;

    return (
      <Page
        isLoading={isUserInfoLoading}
        errorMessage={errorMessage}
        component={<Profile userInfo={userInfo} />}
      />
    );
  }
}

ProfilePage.propTypes = {
  userId: PropTypes.number.isRequired,
  isAuthorized: PropTypes.bool.isRequired,
  isUserInfoLoading: PropTypes.bool.isRequired,
  getUserInfo: PropTypes.func.isRequired,
  userInfo: PropTypes.objectOf(PropTypes.any).isRequired,
  errorMessage: PropTypes.string.isRequired,
};

const mapStateToProps = state => {
  return {
    userId: state.user.userId,
    isAuthorized: state.user.isAuthorized,
    isUserInfoLoading: state.user.isUserInfoLoading,
    userInfo: state.user.userInfo,
    errorMessage: state.user.errorMessage,
  };
};

const mapDispatchToProps = {
  getUserInfo,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(ProfilePage);
