const messages = new Map();

messages.set('wrong_email_or_password', 'Имя пользователя или пароль введены не верно.');
messages.set('server_unavailable', 'Сервер не доступен.');
messages.set('unknown_error', 'Неизвестная ошибка.');
messages.set('user_not_found', 'Пользователь не найден.');

export const getMessage = (messageId = '') => {
  if (typeof messageId !== 'string') {
    return '';
  }
  return messages.get(messageId.toLowerCase()) || '';
};
