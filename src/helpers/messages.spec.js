import {getMessage} from './messages';

describe('messages', () => {
  it('wrong email or password', () => {
    expect(getMessage('wrong_email_or_password')).toEqual('Имя пользователя или пароль введены не верно.');
  });

  it('server_unavailable', () => {
    expect(getMessage('server_unavailable')).toEqual('Сервер не доступен.');
  });

  it('unknown_error', () => {
    expect(getMessage('unknown_error')).toEqual('Неизвестная ошибка.');
  });

  it('user_not_found', () => {
    expect(getMessage('user_not_found')).toEqual('Пользователь не найден.');
  });

  it('not exist messageId', () => {
    expect(getMessage('123-3345')).toEqual('');
  });

  it('error message in upper case', () => {
    expect(getMessage('WRONG_EMAIL_OR_PASSWORD')).toEqual('Имя пользователя или пароль введены не верно.');
  });

  it('undefined message', () => {
    expect(getMessage(undefined)).toEqual('');
  });

  it('null message', () => {
    expect(getMessage(null)).toEqual('');
  });

  it('wrong messageId type', () => {
    expect(getMessage(123)).toEqual('');
    expect(getMessage({})).toEqual('');
  });
});
