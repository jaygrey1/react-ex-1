import {
  NEWS_REQUEST,
  NEWS_SUCCESS,
  NEWS_FAILURE,
} from '../actions/news';

const initialState = {
  news: null,
  isLoading: false,
  errorMessage: '',
};

export const newsReducer = (state = initialState, action) => {
  switch (action.type) {
    case NEWS_REQUEST: {
      return {
        ...state,
        isLoading: true,
      };
    }
    case NEWS_SUCCESS: {
      return {
        ...state,
        isLoading: false,
        news: action.payload,
        errorMessage: '',
      };
    }
    case NEWS_FAILURE: {
      return {
        ...state,
        isLoading: false,
        errorMessage: action.payload,
      };
    }
    default:
      return { ...state };
  }
};
