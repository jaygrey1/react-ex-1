import { newsReducer } from './news';
import * as t from '../actions/news';

describe('news reducer', () => {
  const initialState = {
    news: null,
    isLoading: false,
    errorMessage: '',
  };

  it('correctly reduce initial state', () => {
    expect(newsReducer(undefined, {})).toEqual(initialState);
  });

  it('NEWS_REQUEST', () => {
    const action = {
      type: t.NEWS_REQUEST,
    };

    expect(newsReducer(initialState, action)).toEqual({ ...initialState, isLoading: true });
  });

  it('NEWS_SUCCESS with 0 news', () => {
    const action = {
      type: t.NEWS_SUCCESS,
      payload: [],
    };

    const currentStore = {
      news: null,
      isLoading: true,
      errorMessage: '',
    };

    expect(newsReducer(currentStore, action)).toEqual({
      ...initialState,
      news: [],
      isLoading: false,
    });
  });

  it('NEWS_SUCCESS with more than zero news', () => {
    const action = {
      type: t.NEWS_SUCCESS,
      payload: [1, 2, 3],
    };

    const currentStore = {
      news: null,
      isLoading: true,
      errorMessage: '',
    };

    expect(newsReducer(currentStore, action)).toEqual({
      ...initialState,
      news: action.payload,
      isLoading: false,
    });
  });

  it('NEWS_SUCCESS, with error before', () => {
    const action = {
      type: t.NEWS_SUCCESS,
      payload: [1, 2, 3],
    };

    const currentState = {
      news: null,
      isLoading: true,
      errorMessage: 'some error',
    };

    expect(newsReducer(currentState, action)).toEqual({
      ...initialState,
      news: [1, 2, 3],
      isLoading: false,
      errorMessage: '',
    });
  });

  it('NEWS_FAILURE', () => {
    const currentState = {
      news: null,
      isLoading: true,
      errorMessage: '',
    };
    const action = {
      type: t.NEWS_FAILURE,
      payload: 'error message',
    };

    expect(newsReducer(currentState, action)).toEqual({
      ...initialState,
      isLoading: false,
      errorMessage: action.payload,
    });
  });

  it('NEWS_FAILURE, with error before', () => {
    const currentState = {
      news: null,
      isLoading: true,
      errorMessage: 'previous error',
    };
    const action = {
      type: t.NEWS_FAILURE,
      payload: 'error message',
    };

    expect(newsReducer(currentState, action)).toEqual({
      ...initialState,
      isLoading: false,
      errorMessage: action.payload,
    });
  });
});
