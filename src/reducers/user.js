import {
  USER_LOGIN_REQUEST,
  USER_LOGIN_SUCCESS,
  USER_LOGIN_FAILURE,
  USER_LOGOUT,
  USER_INFO_REQUEST,
  USER_INFO_SUCCESS,
  USER_INFO_FAILURE,
} from '../actions/user';

const initialState = {
  userId: 0,
  isLoading: false,
  isAuthorized: false,
  errorMessage: '',

  isUserInfoLoading: false,
  userInfo: {},
};

export const userReducer = (state = initialState, action) => {
  switch (action.type) {
    case USER_LOGIN_REQUEST: {
      return { ...state, isLoading: true, errorMessage: '' };
    }
    case USER_LOGIN_SUCCESS: {
      return {
        ...state,
        userId: action.payload.id,
        isLoading: false,
        errorMessage: '',
        isAuthorized: true,
      };
    }
    case USER_LOGIN_FAILURE: {
      return { ...state, isLoading: false, errorMessage: action.payload };
    }
    case USER_LOGOUT: {
      return { ...initialState };
    }
    case USER_INFO_REQUEST: {
      return { ...state, isUserInfoLoading: true, errorMessage: '' };
    }
    case USER_INFO_SUCCESS: {
      return { ...state, isUserInfoLoading: false, userInfo: action.payload };
    }
    case USER_INFO_FAILURE: {
      return { ...state, isUserInfoLoading: false, errorMessage: action.payload };
    }
    default: {
      return state;
    }
  }
};
