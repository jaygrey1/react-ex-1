import { userReducer } from './user';
import * as t from '../actions/user';

describe('user reducers', () => {
  it('test initial state', () => {
    const initialState = {
      userId: 0,
      isLoading: false,
      isAuthorized: false,
      errorMessage: '',

      isUserInfoLoading: false,
      userInfo: {},
    };
    expect(userReducer(undefined, {})).toEqual(initialState);
  });

  describe('user login', () => {
    it('test login request', () => {
      const currentState = {
        userId: 0,
        isLoading: false,
        isAuthorized: false,
        errorMessage: '',
        isUserInfoLoading: false,
        userInfo: {},
      };
      const action = { type: t.USER_LOGIN_REQUEST };

      expect(userReducer(currentState, action)).toEqual({
        ...currentState,
        isLoading: true,
      });
    });

    it('test login success', () => {
      const currentState = {
        userId: 0,
        isLoading: true,
        isAuthorized: false,
        errorMessage: '',
        isUserInfoLoading: false,
        userInfo: {},
      };
      const action = { type: t.USER_LOGIN_SUCCESS, payload: { id: 1 } };

      expect(userReducer(currentState, action)).toEqual({
        ...currentState,
        isLoading: false,
        errorMessage: '',
        userId: 1,
        isAuthorized: true,
      });
    });

    it('test login success, after error', () => {
      const currentState = {
        userId: 0,
        isLoading: true,
        isAuthorized: false,
        errorMessage: 'some error',
        isUserInfoLoading: false,
        userInfo: {},
      };
      const action = { type: t.USER_LOGIN_SUCCESS, payload: { id: 1 } };

      expect(userReducer(currentState, action)).toEqual({
        ...currentState,
        isLoading: false,
        errorMessage: '',
        userId: 1,
        isAuthorized: true,
      });
    });

    it('test login failure', () => {
      const currentState = {
        userId: 0,
        isLoading: true,
        isAuthorized: false,
        errorMessage: '',
        isUserInfoLoading: false,
        userInfo: {},
      };

      const action = { type: t.USER_LOGIN_FAILURE, payload: 'some error' };

      expect(userReducer(currentState, action)).toEqual({
        ...currentState,
        isLoading: false,
        errorMessage: 'some error',
      });
    });
  });

  describe('user info', () => {
    it('user info request', () => {
      const currentState = {
        userId: 1,
        isLoading: false,
        isAuthorized: true,
        errorMessage: '',

        isUserInfoLoading: false,
        userInfo: {},
      };
      const action = { type: t.USER_INFO_REQUEST };

      expect(userReducer(currentState, action)).toEqual({
        ...currentState,
        isUserInfoLoading: true,
      });
    });

    it('user info success', () => {
      const currentState = {
        userId: 1,
        isLoading: false,
        isAuthorized: true,
        errorMessage: '',

        isUserInfoLoading: true,
        userInfo: {},
      };
      const action = { type: t.USER_INFO_SUCCESS, payload: { id: 1 } };

      expect(userReducer(currentState, action)).toEqual({
        ...currentState,
        isUserInfoLoading: false,
        userInfo: { id: 1 },
      });
    });

    it('user info failure', () => {
      const currentState = {
        userId: 1,
        isLoading: false,
        isAuthorized: true,
        errorMessage: '',

        isUserInfoLoading: true,
        userInfo: {},
      };
      const action = { type: t.USER_INFO_FAILURE, payload: 'user info error' };

      expect(userReducer(currentState, action)).toEqual({
        ...currentState,
        isUserInfoLoading: false,
        errorMessage: 'user info error',
      });
    });
  });
});
