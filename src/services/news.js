const server = process.env.REACT_APP_BACKEND_HOST;

export const getAll = async () => {
  const endpoint = '/api/v1/news';
  const headers = new Headers();

  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');

  const response = await fetch(`${server}${endpoint}`, {
    method: 'GET',
    headers,
  });

  return response.json();
};
