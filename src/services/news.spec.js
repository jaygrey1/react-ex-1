import fetchMock from 'fetch-mock';
import {getAll} from './news';

describe('news service', () => {
  const endpoint = 'path:/api/v1/news';

  it('check method and endpoint', async () => {
    fetchMock.getOnce(endpoint, {});

    await getAll();
    
    const options = fetchMock.lastOptions();
    expect(options.method).toEqual('GET');
    
    const url = fetchMock.lastUrl();
    expect(url).toContain('/api/v1/news');

  });
});
