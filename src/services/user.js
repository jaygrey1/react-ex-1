const server = process.env.REACT_APP_BACKEND_HOST;

export const login = async (email, password) => {
  const endpoint = '/api/v1/validate';
  const headers = new Headers();

  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');

  const response = await fetch(`${server}${endpoint}`, {
    method: 'POST',
    headers,
    body: JSON.stringify({
      email,
      password,
    }),
  });

  if (response.ok) {
    return response.json();
  }

  return {
    status: 'err',
    message: 'server_unavailable',
  };
};

export const getUserInfo = async userId => {
  const endpoint = '/api/v1/user-info';
  const headers = new Headers();

  headers.append('Content-Type', 'application/json');
  headers.append('Accept', 'application/json');

  const response = await fetch(`${server}${endpoint}/${userId}`, {
    method: 'GET',
    headers,
  });

  if (response.ok) {
    return response.json();
  }

  return {
    status: 'err',
    message: 'server_unavailable',
  };
};
