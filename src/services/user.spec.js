import fetchMock from 'fetch-mock';
import { login, getUserInfo } from './user';

describe('user services tests', () => {
  afterEach(() => {
    fetchMock.restore();
  });

  describe('login service', () => {
    const endpoint = 'path:/api/v1/validate';

    it('check method endpoint and payload', async () => {
      fetchMock.postOnce(endpoint, {});

      await login('abc@def.com', '12345');

      const options = fetchMock.lastOptions();
      const url = fetchMock.lastUrl();
      const expectedBody = JSON.stringify({ email: 'abc@def.com', password: '12345' });

      expect(fetchMock.calls()).toHaveLength(1);
      expect(options.method).toEqual('POST');
      expect(url).toContain('/api/v1/validate');
      expect(options.body).toBe(expectedBody);
    });

    it('check returned value on success', async () => {
      const expectedResult = {
        status: 'ok',
        data: { id: 1 },
      };

      fetchMock.postOnce(endpoint, { status: 200, body: expectedResult });

      const result = await login('abc@def.com', '12345');
      expect(result).toEqual(expectedResult);
    });

    it('check returned value on failure', async () => {
      fetchMock.postOnce(endpoint, { status: 400 });
      const result = await login('abc@def.com', '12345');
      expect(result).toEqual({ status: 'err', message: 'server_unavailable' });
    });
  });

  describe('user info service', () => {
    const endpoint = 'express:/api/v1/user-info/:id';

    it('check method and endpoint', async () => {
      fetchMock.getOnce(endpoint, { status: 200, body: {} });

      await getUserInfo(1);

      const options = fetchMock.lastOptions();
      const url = fetchMock.lastUrl();

      expect(fetchMock.calls()).toHaveLength(1);
      expect(options.method).toBe('GET');
      expect(url).toContain('/api/v1/user-info/1');
    });

    it('check returned value on success', async () => {
      const expectedBody = {
        status: 'ok',
        data: {
          userId: 1,
          city: 'some city',
          languages: ['language 1', 'language 2'],
          social: [{ label: 'label 1', link: 'link1' }],
        },
      };

      fetchMock.getOnce(endpoint, { status: 200, body: expectedBody });

      const result = await getUserInfo(1);

      expect(result).toEqual(expectedBody);
    });

    it('check returned value on failure', async () => {
      const expectedResult = {
        status: 'err',
        message: 'server_unavailable',
      };
      fetchMock.getOnce(endpoint, { status: 400, body: {} });

      const result = await getUserInfo(2);
      expect(result).toEqual(expectedResult);
    });
  });
});
