import { createStore, combineReducers, applyMiddleware } from 'redux';
import thunk from 'redux-thunk';
import logger from 'redux-logger';

import { userReducer } from '../reducers/user';
import { newsReducer } from '../reducers/news';

const rootReducer = combineReducers({ user: userReducer, news: newsReducer });

export const store = createStore(rootReducer, applyMiddleware(thunk, logger));
